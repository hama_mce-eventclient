HAMAMCE_OBJS = hama_mce.o
HAMAMCE_EXE = hama_mce

ifeq ($(strip $(DEBUG)),Y)
  CXXFLAGS += -g -DDEBUG
else
  ifndef CXXFLAGS
    CXXFLAGS += -O2
  endif
  LDFLAGS += -s
endif

CXXFLAGS += -Wall -Werror -pipe -ansi

# ---------------------------------------------------------------------
#  libUSB
# ---------------------------------------------------------------------
CXXFLAGS += -I3rdparty/include
LDFLAGS += -L3rdparty/lib
LIBS = -lusb-1.0 -lrt


all : $(HAMAMCE_EXE)

update:
	install -m755 $(HAMAMCE_EXE) /usr/sbin

install:
	install -m755 $(HAMAMCE_EXE) /usr/sbin
	install -m644 hama_mce.rules /etc/udev/rules.d
	install -m644 99_hama_mce /etc/pm/sleep.d
	install -m644 hama_mce.xml $(HOME)/.xbmc/userdata/keymaps

$(HAMAMCE_EXE) : $(HAMAMCE_OBJS)
	$(CXX) $(CFLAGS) $(LDFLAGS) -o $@ $^ $(LIBS)

$(HAMAMCE_OBJS) : 3rdparty/include/libusb-1.0/libusb.h

3rdparty/include/libusb-1.0/libusb.h :
	( cd ./3rdparty && make -f Makefile.libusb-1.0 )

.PHONY : clean
clean :
	-rm $(HAMAMCE_EXE) $(HAMAMCE_OBJS)

.PHONY : clean.all
clean.all : clean
	-rm -r 3rdparty/libusb-1.0.*.stamp 3rdparty/include 3rdparty/lib
